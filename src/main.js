import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Axios from "axios";
import "@/assets/css/index.css";
import DatePicker from "vue2-datepicker";

import PerfectScrollbar from "vue2-perfect-scrollbar";
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";

Vue.config.productionTip = false;

Vue.use(PerfectScrollbar);
Vue.use(DatePicker);
new Vue({
  router,
  store,
  Axios,
  render: (h) => h(App),
}).$mount("#app");
